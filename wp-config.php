<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'creatologydb');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '4#@#/+eu{?~jS|Y)-k6EM!+){}f1uyP3_4(*AO3vD/Fo{p7l?C0+aRm64*)+:]B>');
define('SECURE_AUTH_KEY',  'mV!}kkSq6u4Exz)qF4*ouXyWyg[O+Pj:iN7R^ SGvttjOf|] j)y3l8/%G;N;@rE');
define('LOGGED_IN_KEY',    '7R`3S_>|$?:8~1qt(-HOCw`L}jziXwcME|4!7_fh|rHN$ELzzbd$0+/(t;!_(Mp5');
define('NONCE_KEY',        'Kp8ZG55Bh|eK*[Qg+B&jwJS*&xg3fND%#BPHFY)unL3Tt*Y0X/NZ=Q4kTGL6n^*$');
define('AUTH_SALT',        '4ucC7~~|~Fb8^MNd<k{+vT@mv-7Wmn6k5nX/G(-[HN++|1mXoxtZ-JpMb{_!:!r2');
define('SECURE_AUTH_SALT', 'WpQtvY!n#?-I_!u-XPA(G>R)%vKcb0))D}AT@`4m.%|dh.}d6_RP9-(24#[d#0dn');
define('LOGGED_IN_SALT',   'UaQ9@:j-W{F!j-v?K@OuVU02 Aq6fF&H9v%NHklE+y))CK-jPu(BBS`wtYogy}>j');
define('NONCE_SALT',       '?NniPx;pW{*GV^-)n-s];SAj-rTlf+*a+-ZhM5(sk+Or%|(kdB^#eJ7XU5-bo)W-');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'crt_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
